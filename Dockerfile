FROM maven:3.6-jdk-13
MAINTAINER podcherklife@gmail.com
ADD . /wages
RUN ["mvn", "package", "-f", "wages/pom.xml"]
CMD ["java", "-jar", "wages/target/wages.jar"]

EXPOSE 8080/tcp
EXPOSE 8080/udp