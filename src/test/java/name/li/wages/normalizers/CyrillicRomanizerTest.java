package name.li.wages.normalizers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CyrillicRomanizerTest {

	@Test
	public void test() {
		var romanizer = new CyrillicRomanizer();
		assertEquals("moskva", romanizer.romanize("москва"));
		assertEquals("hello", romanizer.romanize("hello"));
		assertEquals("privet mirschch!", romanizer.romanize("привет мирщч!"));
	}

}
