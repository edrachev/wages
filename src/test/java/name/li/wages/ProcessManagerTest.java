package name.li.wages;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.persistence.GenerationRepository;
import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Generation.Status;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;

public class ProcessManagerTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private GenerationRepository generationRepository;

	@Mock
	private ProcessDescriptor process;

	@Mock
	private Step step;

	@Mock
	private PlatformTransactionManager transactionManager;

	private ProcessManager pm;

	private String PROCESS_ID = "process_id";

	@Before
	public void init() {
		when(process.createExecution(any(), any(), any())).thenReturn(step);
		when(process.id()).thenReturn(PROCESS_ID);
		when(step.status()).thenReturn(GeneratorStatus.simple(() -> 100));

		pm = new ProcessManager(
				Collections.singleton(process),
				generationRepository,
				transactionManager);
	}

	@Test
	public void testCreateExecutionForNonExistantProcess() {
		Assert.assertThrows(IllegalArgumentException.class, () -> pm.createNewExecution("bar"));
	}

	@Test
	public void testStatusForNonExistantProcess() {
		assertThat(pm.info("bar").isPresent()).isFalse();
	}

	@Test
	public void testCancelExecution() {
		var mostRecentGenration = generation(PROCESS_ID, Status.FAILED, token("step", "value"));
		var lastCompletedGenration = generation(PROCESS_ID, Status.COMPLETED, null);
		setUpPreviousExecutions(PROCESS_ID, mostRecentGenration, lastCompletedGenration);

		pm.tryContinueExecution(PROCESS_ID);
		pm.cancelUnfinishedExecution(PROCESS_ID);

		assertThat(mostRecentGenration.isCancelled());
		assertThat(mostRecentGenration.getContinuationToken().isEmpty()).isTrue();

		Assert.assertThrows(IllegalStateException.class, () -> pm.cancelUnfinishedExecution(PROCESS_ID));
	}

	@Test
	public void testCreateTheVeryFirstExecution() {
		setUpPreviousExecutions(PROCESS_ID, null, null);

		pm.createNewExecution(PROCESS_ID);
		var status = pm.info(PROCESS_ID);

		verify(process).createExecution(eq(Optional.empty()), eq(Optional.empty()), any());
		verify(generationRepository).save(any(Generation.class));
		assertThat(status.isPresent()).isTrue();
	}

	@Test
	public void testCreateExecutionToContinue() {
		var mostRecentGenration = generation(PROCESS_ID, Status.FAILED, token("step", "value"));
		var lastCompletedGenration = generation(PROCESS_ID, Status.COMPLETED, null);
		setUpPreviousExecutions(PROCESS_ID, mostRecentGenration, lastCompletedGenration);

		pm.tryContinueExecution(PROCESS_ID);

		verify(process).createExecution(
				Optional.of(token("step", "value")),
				Optional.of(lastCompletedGenration),
				mostRecentGenration);
		verify(generationRepository).save(mostRecentGenration);
	}

	@Test
	public void testContinueExecutionOk() {
		var mostRecentGenration = generation(PROCESS_ID, Status.FAILED, token("step", "value"));
		var lastCompletedGenration = generation(PROCESS_ID, Status.COMPLETED, null);
		setUpPreviousExecutions(PROCESS_ID, mostRecentGenration, lastCompletedGenration);
		pm.tryContinueExecution(PROCESS_ID);

		pm.execute(PROCESS_ID);

		verify(step).start();
		assertThat(mostRecentGenration.isCompleted()).isTrue();
		assertThat(mostRecentGenration.getAttempts()).hasSize(1);
		assertThat(mostRecentGenration.getContinuationToken().isEmpty()).isTrue();

		Assert.assertThrows(IllegalArgumentException.class, () -> pm.execute(PROCESS_ID));
	}

	@Test
	public void testContinueExecutionFailedWithProcessException() {
		var mostRecentGenration = generation(PROCESS_ID, Status.FAILED, token("step", "value"));
		var lastCompletedGenration = generation(PROCESS_ID, Status.COMPLETED, null);
		setUpPreviousExecutions(PROCESS_ID, mostRecentGenration, lastCompletedGenration);
		pm.tryContinueExecution(PROCESS_ID);
		doThrow(new Step.ProcessError(token("process", "value"), new RuntimeException())).when(step).start();

		pm.execute(PROCESS_ID);

		verify(step).start();
		assertThat(mostRecentGenration.isFailed()).isTrue();
		assertThat(mostRecentGenration.getAttempts()).hasSize(1);
		assertThat(mostRecentGenration.getContinuationToken()).isEqualTo(Optional.of(token("process", "value")));

		Assert.assertThrows(IllegalArgumentException.class, () -> pm.execute(PROCESS_ID));
	}

	@Test
	public void testContinueExecutionFailedWithUnknownError() {
		var mostRecentGenration = generation(PROCESS_ID, Status.FAILED, token("step", "value"));
		var lastCompletedGenration = generation(PROCESS_ID, Status.COMPLETED, null);
		setUpPreviousExecutions(PROCESS_ID, mostRecentGenration, lastCompletedGenration);
		pm.tryContinueExecution(PROCESS_ID);
		doThrow(new RuntimeException()).when(step).start();

		pm.execute(PROCESS_ID);

		verify(step).start();
		assertThat(mostRecentGenration.isFailed()).isTrue();
		assertThat(mostRecentGenration.getAttempts()).hasSize(1);
		assertThat(mostRecentGenration.getContinuationToken().isEmpty()).isEqualTo(false);

		Assert.assertThrows(IllegalArgumentException.class, () -> pm.execute(PROCESS_ID));
	}

	private void setUpPreviousExecutions(
			String processId,
			Generation lastGeneration,
			Generation lastSuccessfulGeneration) {
		when(generationRepository.getMostRecentGeneration(processId))
				.thenReturn(Optional.ofNullable(lastGeneration));
		when(generationRepository.getLastCompletedGeneration(processId))
				.thenReturn(Optional.ofNullable(lastSuccessfulGeneration));

	}

	private Generation generation(String description, Generation.Status status, ContinuationToken token) {
		var result = new Generation(description);
		result.setStatus(status);
		if (token != null) {
			result.setContinuationToken(token);
		}
		return result;
	}

	private ContinuationToken token(String process, String value) {
		return ContinuationToken.builder().setStepId(process).setValue(value).build();
	}

}
