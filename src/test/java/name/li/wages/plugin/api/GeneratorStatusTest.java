package name.li.wages.plugin.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

public class GeneratorStatusTest {

	@Test
	public void simpleStatusNotWrokingIfMinus1PercentCompleted() {
		var val = new AtomicInteger();
		val.set(0);

		var status = GeneratorStatus.simple(() -> val.get());
		assertTrue(status.working());
		assertEquals(status.percentsCompleted(), 0);

		val.set(-1);

		assertFalse(status.working());
		assertEquals(status.percentsCompleted(), -1);
	}

	@Test
	public void simpleStatusFinishesWorkWhen100PercentCompleted() {
		var val = new AtomicInteger();
		val.set(0);

		var status = GeneratorStatus.simple(() -> val.get());
		assertTrue(status.working());
		assertEquals(status.percentsCompleted(), 0);

		val.set(100);

		assertFalse(status.working());
		assertEquals(status.percentsCompleted(), 100);
	}

	@Test
	public void compositeStatusAvegaresSubstatuses() {

		var val1 = new AtomicInteger();
		var val2 = new AtomicInteger();

		var status = GeneratorStatus.composite("compositeexample", Arrays.asList(
				GeneratorStatus.simple(() -> val1.get()),
				GeneratorStatus.simple(() -> val2.get())));

		assertTrue(status.working());
		assertEquals(status.percentsCompleted(), 0);

		val1.set(100);

		assertTrue(status.working());
		assertEquals(status.percentsCompleted(), 50);

		val2.set(100);

		assertFalse(status.working());
		assertEquals(status.percentsCompleted(), 100);

	}

}
