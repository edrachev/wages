package name.li.wages.plugin.japandevcom;

import static com.google.common.truth.Truth.assertThat;

import java.util.Optional;

import org.junit.Test;

import name.li.wages.plugin.api.Location;

public class JapanDevLocationParserTest {

	@Test
	public void testParsing() {
		JapanDevLocationParser parser = new JapanDevLocationParser();

		assertThat(parser.parse("Tokyo (Tamachi)"))
				.isEqualTo(Optional.of(Location.create("japan", "Tokyo", "Tamachi")));
		assertThat(parser.parse("Tokyo"))
				.isEqualTo(Optional.of(Location.create("japan", "Tokyo", "")));
		assertThat(parser.parse("")).isEqualTo(Optional.empty());
	}

}
