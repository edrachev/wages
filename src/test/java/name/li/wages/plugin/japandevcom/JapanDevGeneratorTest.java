package name.li.wages.plugin.japandevcom;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.OptionalInt;
import java.util.stream.Collectors;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.Sets;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevCompany;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJob;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevJobAttributes;
import name.li.wages.plugin.japandevcom.dataclasses.JapanDevResponse;

public class JapanDevGeneratorTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private JapanDevClient japanDevClient;

	@Test
	public void testResponseConversion() throws JsonParseException, JsonMappingException, IOException {
		when(japanDevClient.getData()).thenReturn(JapanDevResponse.builder()
				.setData(Arrays.asList(
						JapanDevJob.builder()
								.setId(99)
								.setType("job")
								.setAttributes(JapanDevJobAttributes.builder()
										.setTitle("developer")
										.setCompany(JapanDevCompany.builder().setName("yakuza").build())
										.setLocation("Tokyo (Otemachi)")
										.setTechnologies("spring, java")
										.setSalaryMax(100)
										.setSalaryMin(50)
										.setSlug("slug")
										.setJobPostDate("Dec 18th 2019")
										.build())
								.build(),
						JapanDevJob.builder()
								.setId(100)
								.setType("job")
								.setAttributes(JapanDevJobAttributes.builder()
										.setTitle("bigboss")
										.setCompany(JapanDevCompany.builder().setName("company").build())
										.setLocation("Tokyo")
										.setTechnologies("foo, bar")
										.setSlug("otherslug")
										.setJobPostDate("2019-10-16")
										.build())
								.build(),
						JapanDevJob.builder()
								.setId(101)
								.setType("job")
								.setAttributes(JapanDevJobAttributes.builder()
										.setTitle("example")
										.setCompany(JapanDevCompany.builder().setName("company").build())
										.setLocation("Tokyo (Otemachi)")
										.setTechnologies("foo, bar")
										.setSlug("otherslug")
										.setJobPostDate("2019-10-16")
										.build())
								.build()))
				.build());

		JapanDevParser generator = new JapanDevParser(japanDevClient);

		var result = generator.generate().collect(Collectors.toList());

		assertThat(result).hasSize(2);
		var item1 = result.get(0);
		assertThat(item1.getParsingProblems()).isEmpty();
		assertThat(item1.getLocation()).isEqualTo(Location.create("japan", "Tokyo", "Otemachi"));
		assertThat(item1.getBuzzwords()).isEqualTo(Sets.newHashSet(Buzzword.of("spring"), Buzzword.of("java")));
		assertThat(item1.getMinSalary()).isEqualTo(OptionalInt.of(5_000_000));
		assertThat(item1.getMaxSalary()).isEqualTo(OptionalInt.of(10_000_000));
		assertThat(item1.getExternalId()).isEqualTo("japan-dev.com/jobs/yakuza/slug");
		assertThat(item1.getCompany()).isEqualTo("yakuza");
		assertThat(item1.getPosition()).isEqualTo("developer");
		assertThat(item1.getSalaryCurrency()).isEqualTo("yen");
		assertThat(item1.getCreatedOn()).isEqualTo(LocalDate.of(2019, 12, 18));

		assertThat(result).hasSize(2);
		var item2 = result.get(1);
		assertThat(item2.getParsingProblems()).isEmpty();
		assertThat(item2.getLocation()).isEqualTo(Location.create("japan", "Tokyo", ""));
		assertThat(item2.getBuzzwords()).isEqualTo(Sets.newHashSet(Buzzword.of("foo"), Buzzword.of("bar")));
		assertThat(item2.getMinSalary()).isEqualTo(OptionalInt.empty());
		assertThat(item2.getMaxSalary()).isEqualTo(OptionalInt.empty());
		assertThat(item2.getExternalId()).isEqualTo("japan-dev.com/jobs/company/otherslug");
		assertThat(item2.getCompany()).isEqualTo("company");
		assertThat(item2.getPosition()).isEqualTo("bigboss");
		assertThat(item2.getSalaryCurrency()).isEqualTo("yen");
		assertThat(item2.getCreatedOn()).isEqualTo(LocalDate.of(2019, 10, 16));
	}

}
