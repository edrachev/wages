package name.li.wages.plugin.hh.process;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.hh.HhRawJobEntry;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;
import name.li.wages.plugin.hh.HhSearchInProgressParser;

public class HhSearchParsingStepTest {
	private static final String STEP_ID = "stepId";
	@Rule
	public MockitoRule mockito = MockitoJUnit.rule();
	@Mock
	private HhRawJobEntryRepository rawJobEntryRepository;
	@Mock
	private PlatformTransactionManager tm;
	@Mock
	private HhSearchInProgressParser parser;

	private Generation generation;

	private HhSearchParsingStep parsingStep;

	@Before
	public void init() {
		this.generation = Generation.create("generation");
		this.parsingStep = new HhSearchParsingStep(STEP_ID, rawJobEntryRepository, parser, tm, generation);
	}

	@Test
	public void testEmptyGeneration() {
		when(parser.generate()).thenReturn(Stream.of());

		parsingStep.start();
	}

	@Test
	public void testNormalGeneration() {
		var entry = new HhRawJobEntry();
		when(parser.generate()).thenReturn(Stream.of(entry));

		parsingStep.start();

		verify(rawJobEntryRepository).save(entry);
		assertThat(entry.getGeneration()).isEqualTo(generation);
	}

	@Test
	public void testFailedGeneration() {
		var parserException = new RuntimeException();
		doThrow(parserException).when(parser).generate();

		var exception = assertThrows(Step.ProcessError.class, () -> parsingStep.start());

		assertThat(exception.getCause())
				.isEqualTo(parserException);
		assertThat(exception.getToken())
				.isEqualTo(ContinuationToken.builder().setStepId(STEP_ID).setValue(STEP_ID).build());
	}

}
