package name.li.wages.plugin.hh;

import static org.junit.Assert.assertEquals;

import java.util.Optional;
import java.util.OptionalInt;

import org.junit.Test;

import name.li.wages.plugin.api.Salary;

public class SalaryParserTest {

	private SalaryParser parser = new SalaryParser();

	@Test
	public void testIntervalParsing() {
		assertEquals(Optional.empty(), parser.parse("Q__100 - 200 RUB"));
		assertEquals(parsingResult(100, 200, "RUB"), parser.parse("100 - 200 RUB"));
		assertEquals(parsingResult(300_000, 500_000, "EUR"), parser.parse("300 00 0 -50 0000EUR"));

		assertEquals(parsingResult(50_000, 100_000, "руб"), parser.parse("50 000-100 000 руб."));
	}

	@Test
	public void testRightOpenInterval() {
		assertEquals(Optional.empty(), parser.parse("отидо100 RUB"));
		assertEquals(parsingResult(100, 0, "RUB"), parser.parse("от 100 RUB"));
		assertEquals(parsingResult(100_000, 0, "EUR"), parser.parse("от 100 000 EUR"));

		assertEquals(parsingResult(120_000, 0, "руб"), parser.parse("от 120 000 руб."));
	}

	@Test
	public void testLeftOpenInterval() {
		assertEquals(Optional.empty(), parser.parse("отидо100 RUB"));
		assertEquals(parsingResult(0, 100, "RUB"), parser.parse("до100 RUB"));
		assertEquals(parsingResult(0, 100_000, "EUR"), parser.parse("до100 000 EUR"));

		assertEquals(parsingResult(0, 120_000, "руб"), parser.parse("до 120 000 руб."));
	}

	@Test
	public void testUndefined() {
		assertEquals(Optional.of(Salary.builder().build()), parser.parse("з/п не указана"));
	}

	private Optional<Salary> parsingResult(int min, int max, String currency) {
		return Optional.of(Salary.builder()
				.setMinSalary(min == 0 ? OptionalInt.empty() : OptionalInt.of(min))
				.setMaxSalary(max == 0 ? OptionalInt.empty() : OptionalInt.of(max))
				.setCurrency(currency).build());
	}

}
