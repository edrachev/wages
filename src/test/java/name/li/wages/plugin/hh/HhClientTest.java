package name.li.wages.plugin.hh;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.openqa.selenium.remote.RemoteWebDriver;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.hh.HhClient.Location;
import name.li.wages.plugin.hh.HhClient.SearchConfiguration;
import name.li.wages.selenium.SeleniumProvider;

public class HhClientTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private HhSearchParserFactory parserFactory;
	@Mock
	private SeleniumProvider selenium;
	@Mock
	private RemoteWebDriver driver;
	@Mock
	private HhConfiguration hhConfig;

	private HhClient hhClient;

	private SearchConfiguration searchConfig;

	@Before
	public void init() {
		when(selenium.getNewDriver()).thenReturn(driver);
		when(hhConfig.defaultSearchPeriod()).thenReturn(2L);
		hhClient = new HhClient(selenium, parserFactory, hhConfig);
		searchConfig = SearchConfiguration.builder()
				.setLocation(Location.RYAZAN)
				.setTerm("truth")
				.build();
	}

	@Test
	public void searchPeriodIsDefaultIfNoLastGenerationPresent() {
		ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);

		hhClient.getSearchResultForLocationAndTerm(
				searchConfig,
				Optional.empty());

		verify(driver).get(urlCaptor.capture());
		assertThat(urlCaptor.getValue()).isEqualTo(
				UriBuilder.fromPath("https://hh.ru/search/vacancy")
						.queryParam("st", "searchVacancy")
						.queryParam("search_period", 2)
						.queryParam("area", Location.RYAZAN.code())
						.queryParam("text", "truth").toString());
	}

	@Test
	public void searchPeriodCalculatedFromLastGeneration() {
		ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);

		hhClient.getSearchResultForLocationAndTerm(
				searchConfig,
				Optional.of(Generation.create("foo")));

		urlCaptor = ArgumentCaptor.forClass(String.class);

		verify(driver).get(urlCaptor.capture());
		assertThat(urlCaptor.getValue()).isEqualTo(
				UriBuilder.fromPath("https://hh.ru/search/vacancy")
						.queryParam("st", "searchVacancy")
						.queryParam("search_period", 1)
						.queryParam("area", Location.RYAZAN.code())
						.queryParam("text", "truth").toString());

	}

}
