package name.li.wages.plugin.hh.process;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertThrows;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.hh.HhEnricher;
import name.li.wages.plugin.hh.HhRawJobEntry;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;

public class HhDetailsParsingStepTest {

	private static final String STEP_ID = "stepId";
	@Rule
	public MockitoRule mockito = MockitoJUnit.rule();
	@Mock
	private HhRawJobEntryRepository rawJobEntryRepository;
	@Mock
	private PlatformTransactionManager tm;
	@Mock
	private HhEnricher enricher;

	private Generation generation;

	private HhDetailsParsingStep parsingStep;

	@Before
	public void init() {
		this.generation = Generation.create("generation");
		this.parsingStep = new HhDetailsParsingStep(rawJobEntryRepository, tm, generation, enricher, STEP_ID);
	}

	@Test
	public void testEmptyGeneration() {
		when(rawJobEntryRepository.getAllEntriesForGeneration(anyString()))
				.thenReturn(Collections.emptyList());
		when(rawJobEntryRepository.getEntriesWithoutDetailsForGeneration(anyString()))
				.thenReturn(Collections.emptyList());

		parsingStep.start();
	}

	@Test
	public void testNormalGeneration() {
		var rawEntry = new HhRawJobEntry();
		when(rawJobEntryRepository.getAllEntriesForGeneration(anyString()))
				.thenReturn(Arrays.asList(rawEntry));
		when(rawJobEntryRepository.getEntriesWithoutDetailsForGeneration(anyString()))
				.thenReturn(Arrays.asList(rawEntry));

		parsingStep.start();

		verify(rawJobEntryRepository).save(rawEntry);
		assertThat(rawEntry.hasExtendedData()).isTrue();
	}

	@Test
	public void testContinuationTokenGeneration() {
		var rawEntry = new HhRawJobEntry();
		when(rawJobEntryRepository.getAllEntriesForGeneration(anyString()))
				.thenReturn(Arrays.asList(rawEntry));
		when(rawJobEntryRepository.getEntriesWithoutDetailsForGeneration(anyString()))
				.thenReturn(Arrays.asList(rawEntry));
		var enricherException = new RuntimeException();
		doThrow(enricherException).when(enricher).enrich(rawEntry);

		var exception = assertThrows(Step.ProcessError.class, () -> parsingStep.start());

		assertThat(exception.getCause())
				.isEqualTo(enricherException);
		assertThat(exception.getToken())
				.isEqualTo(ContinuationToken.builder().setStepId(STEP_ID).setValue(STEP_ID).build());
	}

}
