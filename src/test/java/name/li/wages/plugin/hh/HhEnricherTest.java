package name.li.wages.plugin.hh;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Navigation;
import org.openqa.selenium.WebElement;

import name.li.wages.plugin.api.Buzzword;

public class HhEnricherTest {

	@Rule
	public MockitoRule mockito = MockitoJUnit.rule();

	@Mock
	private WebDriver selenium;
	@Mock
	private Navigation seleniumNavigation;
	@Mock
	private WebElement descriptionElement;

	@Before
	public void init() {
		when(selenium.navigate()).thenReturn(seleniumNavigation);
		when(selenium.findElement(By.className("vacancy-description"))).thenReturn(descriptionElement);
		when(descriptionElement.getText()).thenReturn("long description");
		var skills = Arrays.asList(mockSkillElement("java"), mockSkillElement("spring"));
		when(selenium.findElements(By.cssSelector("[data-qa='skills-element']"))).thenReturn(skills);
	}

	@Test
	public void testLongDescriptionIsFilled() {
		var enricher = new HhEnricher(() -> selenium);
		var entry = new HhRawJobEntry();
		entry.setJobUrl("far.away");

		enricher.enrich(entry);

		verify(seleniumNavigation).to(entry.getJobUrl());
		assertThat(entry.getLongDescription()).isEqualTo("long description");
		assertThat(entry.getBuzzwords()).containsExactly(Buzzword.of("spring"), Buzzword.of("java"));

	}

	private WebElement mockSkillElement(String skill) {
		var mock = mock(WebElement.class);
		when(mock.getText()).thenReturn(skill);
		return mock;
	}

}
