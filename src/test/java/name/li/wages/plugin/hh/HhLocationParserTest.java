package name.li.wages.plugin.hh;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import name.li.wages.plugin.api.Location;

public class HhLocationParserTest {

	@Test
	public void shouldParseHhLocations() {
		var parser = new HhLocationParser("russia");

		assertEquals(Location.create("russia", "Москва", ""),
				parser.parseLocation("Москва"));

		assertEquals(Location.create("russia", "Москва", "Китай-город и еще 1"),
				parser.parseLocation("Москва, Китай-город и еще 1 "));
	}
}
