package name.li.wages.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import name.li.wages.plugin.api.process.Generation;

public interface GenerationRepository extends CrudRepository<Generation, String> {

	@Query("select t from Generation t where t.processId = ?1 order by t.instant desc")
	List<Generation> getGenerations(String processId);

	default Optional<Generation> getMostRecentGeneration(String processId) {
		return getGenerations(processId).stream().findFirst();
	}

	@Query("select t from Generation t where t.processId = ?1 and t.status = 'COMPLETED' order by t.instant desc")
	List<Generation> getCompletedGenerations(String processId);

	default Optional<Generation> getLastCompletedGeneration(String processId) {
		return getCompletedGenerations(processId).stream().findFirst();
	}
}
