package name.li.wages.persistence;

import org.springframework.data.repository.CrudRepository;

import name.li.wages.plugin.api.JobEntry;

public interface JobEntryRepository extends CrudRepository<JobEntry, String> {

}
