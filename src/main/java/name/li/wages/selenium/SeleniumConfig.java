package name.li.wages.selenium;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SeleniumConfig {

	@Value("${selenium.url}")
	private String url;

	@Bean
	public SeleniumProvider seleniumProvider() {
		return new SeleniumProvider(url);
	}

}
