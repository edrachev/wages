package name.li.wages.plugin.hh;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface HhRawJobEntryRepository extends CrudRepository<HhRawJobEntry, String> {

	@Query("select t from HhRawJobEntry t where generation.id = ?1")
	List<HhRawJobEntry> getAllEntriesForGeneration(String generationId);

	@Query("select t from HhRawJobEntry t where t.generation.id = ?1 and hasExtendedData = false")
	List<HhRawJobEntry> getEntriesWithoutDetailsForGeneration(String generationId);

	@Query("select t from HhRawJobEntry t where t.generation.instant in (select max(q.generation.instant) from HhRawJobEntry q group by q.jobUrl )")
	List<HhRawJobEntry> getLastVersionsOfRawEntities();
}
