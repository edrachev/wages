package name.li.wages.plugin.hh;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;

import name.li.wages.plugin.api.BuzzWordFlattener;
import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.process.Generation;

@Entity
@Table(name = "hh_raw_job_entry")
public class HhRawJobEntry {

	@Id
	@Column(name = "id")
	private String id = UUID.randomUUID().toString();

	@ManyToOne
	private Generation generation;

	@Column(name = "searched_by")
	private String searchedBy = "";

	@Column(name = "job_url")
	private String jobUrl = "";

	@Column(name = "salary")
	private String salary = "";

	@Column(name = "title")
	private String title = "";

	@Lob
	@Column(name = "short_description")
	private String shortDescription = "";

	@Lob
	@Column(name = "long_description")
	private String longDescription = "";

	@Column(name = "employer")
	private String employer = "";

	@Column(name = "location")
	private String location = "";

	@Column(name = "country")
	private String country = "";

	@Column(name = "creation_date")
	private String creationDate = "";

	@Convert(converter = BuzzWordFlattener.class)
	@Column(name = "buzzwords")
	private Set<Buzzword> buzzwords = Collections.emptySet();

	@Column(name = "has_extended_data")
	private boolean hasExtendedData = false;

	@ElementCollection
	private Collection<String> parsingProblems = Lists.newArrayList();

	public String getId() {
		return id;
	}

	public Generation getGeneration() {
		return generation;
	}

	public void setGeneration(Generation generation) {
		this.generation = generation;
	}

	public String getJobUrl() {
		return jobUrl;
	}

	public void setJobUrl(String url) {
		this.jobUrl = url;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void addParsingProblem(String problem) {
		parsingProblems.add(problem);
	}

	public Collection<String> getParsingProblems() {
		return Collections.unmodifiableCollection(parsingProblems);
	}

	public void clearParsingProblems() {
		parsingProblems = new ArrayList<>();
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setSearchedBy(String searchedBy) {
		this.searchedBy = searchedBy;
	}

	public String getSearchedBy() {
		return searchedBy;
	}

	public void setBuzzwords(Set<Buzzword> buzzwords) {
		this.buzzwords = buzzwords;
	}

	public Set<Buzzword> getBuzzwords() {
		return buzzwords;
	}

	public boolean hasExtendedData() {
		return hasExtendedData;
	}

	public void setHasExtendedData(boolean hasExtendedData) {
		this.hasExtendedData = hasExtendedData;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("id", id)
				.add("jobUrl", jobUrl)
				.add("country", country)
				.add("location", location)
				.add("salary", salary)
				.add("employer", employer)
				.add("shortDescription", shortDescription)
				.add("longDescription", longDescription)
				.add("title", title)
				.add("creationDate", creationDate)
				.add("parsingProblems", parsingProblems)
				.add("searchedBy", searchedBy)
				.add("skills", buzzwords)
				.add("hasExtendedData", hasExtendedData)
				.toString();
	}

}
