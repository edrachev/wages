package name.li.wages.plugin.hh;

import java.util.regex.Pattern;

import name.li.wages.plugin.api.Location;

public class HhLocationParser {

	private final String country;

	public HhLocationParser(String country) {
		this.country = country;
	}

	public Location parseLocation(String locationString) {
		var loc = new Location();
		loc.setCountry(country);

		var regex = "^\\s*([\\p{L}]*)\\s*,([\\p{L}0-9\\-\\s]*)$"; // "Москва, красногвардейская"
		var matcher = Pattern.compile(regex).matcher(locationString);
		if (matcher.find()) {
			var city = matcher.group(1).trim();
			var area = matcher.group(2).trim();
			loc.setCity(city);
			loc.setArea(area);
			return loc;
		} else {
			loc.setCity(locationString);
			return loc;
		}
	}

}
