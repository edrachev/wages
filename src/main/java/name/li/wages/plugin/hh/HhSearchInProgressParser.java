package name.li.wages.plugin.hh;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.hh.HhClient.SearchConfiguration;

public class HhSearchInProgressParser implements AutoCloseable {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private WebDriver currentSearchPage;
	private Supplier<WebDriver> getNewDriver;
	private AtomicInteger totalPages = new AtomicInteger(0);
	private AtomicInteger processedPages = new AtomicInteger(0);
	private boolean closed = false;

	private final String parserName;
	private final SearchConfiguration searchConfiguration;
	private String initialSearchPageUrl;

	public static HhSearchInProgressParser startSearch(
			WebDriver searchResultPage,
			Supplier<WebDriver> seleniumDriverSupplier,
			SearchConfiguration searchConfiguration) {
		return new HhSearchInProgressParser(searchResultPage, seleniumDriverSupplier, searchConfiguration);
	}

	private HhSearchInProgressParser(WebDriver searchResultPage, Supplier<WebDriver> getNewDriver,
			SearchConfiguration searchConfiguration) {
		this.getNewDriver = getNewDriver;
		this.parserName = String.format(
				"hh parser for location: '%s' and term '%s' ",
				searchConfiguration.location().name(),
				searchConfiguration.term());
		this.searchConfiguration = searchConfiguration;
		initSearch(searchResultPage);
	}

	private void initSearch(WebDriver searchResultPage) {
		this.totalPages.set(getPagesTotal(searchResultPage));
		this.initialSearchPageUrl = searchResultPage.getCurrentUrl();
		searchResultPage.quit();
	}

	private int getPagesTotal(WebDriver searchResultPage) {
		List<WebElement> pageControls = searchResultPage.findElements(By.cssSelector("[data-qa=pager-page]"));
		OptionalInt maxPage = pageControls.stream()
				.mapToInt(e -> Integer.valueOf(Optional
						.ofNullable(e.getAttribute("data-page"))
						.orElse("0")))
				.max();
		return maxPage.orElse(1);
	}

	private void nextPage() {
		assertNotClosed();
		var nextPageButton = getNextPageButton();
		if (nextPageButton.isPresent()) {
			var nextPageUrl = nextPageButton.get().getAttribute("href");
			// the more pages selenium visits the more memory driver consumes
			// so we refresh driver to avoid consumption increasing
			if (processedPages.get() % 5 == 0) {
				logger.debug("Refreshing driver after page {}", processedPages.get());
				this.currentSearchPage.quit();
				this.currentSearchPage = getNewDriver.get();
			}
			currentSearchPage.navigate().to(nextPageUrl);
			processedPages.incrementAndGet();
		} else {
			throw new RuntimeException("Next page button is not there");
		}
	}

	private List<HhRawJobEntry> getAllCurrentPageData() {
		List<WebElement> vacancyBlocks = currentSearchPage.findElements(By.className("vacancy-serp-item"));
		return vacancyBlocks.stream().map(this::createEntryFromVacancyBlock).collect(Collectors.toList());
	}

	private HhRawJobEntry createEntryFromVacancyBlock(WebElement block) {
		HhRawJobEntry entry = new HhRawJobEntry();
		try {
			var company = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-employer]")).getText();
			entry.setEmployer(company);
		} catch (NoSuchElementException e) {
			entry.addParsingProblem("Failed to parse company, cant find element");
		}

		try {
			var rawSalary = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-compensation]"))
					.getText();
			entry.setSalary(rawSalary);
		} catch (NoSuchElementException e) {
			entry.addParsingProblem("Failed to parse salary, cant find element");
		}

		try {
			var link = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-title]")).getAttribute("href");
			var uri = new URI(link);
			entry.setJobUrl(uri.getScheme() + "://" + uri.getHost() + uri.getPath());
		} catch (Exception e) {
			entry.addParsingProblem("Failed to parse link");
		}

		String title = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-title]")).getText();
		String address = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-address]")).getText();
		entry.setTitle(title);
		entry.setLocation(address);
		entry.setCountry(searchConfiguration.location().country());
		entry.setSearchedBy(searchConfiguration.term());
		try {
			var date = block.findElement(By.cssSelector("[data-qa=vacancy-serp__vacancy-date]")).getText();
			entry.setCreationDate(date);
		} catch (Exception e) {
			entry.addParsingProblem("Failed to parse creationDate, cant find element");
		}
		return entry;
	}

	private Optional<WebElement> getNextPageButton() {
		try {
			return Optional.of(currentSearchPage.findElement(By.cssSelector("[data-qa=pager-next]")));
		} catch (NoSuchElementException e) {
			return Optional.empty();
		}
	}

	private void assertNotClosed() {
		if (closed) {
			throw new IllegalStateException("This search is already closed");
		}
	}

	public void close() {
		assertNotClosed();
		this.currentSearchPage.quit();
		closed = true;
	}

	public int getTotalPages() {
		return totalPages.get();
	}

	public int getProcessedPages() {
		return processedPages.get();
	}

	public GeneratorStatus status() {
		return GeneratorStatus.simple(parserName, () -> {
			var total = totalPages.get();
			var processed = processedPages.get();
			return total == 0 ? 100 : (processed * 100) / total;
		});
	}

	public Stream<HhRawJobEntry> generate() {
		assertNotClosed();

		var newWindow = getNewDriver.get();
		newWindow.navigate().to(initialSearchPageUrl);
		this.currentSearchPage = newWindow;

		try {
			Collection<HhRawJobEntry> result = new ArrayList<>();
			logger.debug("Loading all data. Total pages {}", getTotalPages());
			result.addAll(getAllCurrentPageData());
			while (getNextPageButton().isPresent()) {
				nextPage();
				result.addAll(getAllCurrentPageData());
				logger.debug("Finished processing page {} of {}", getProcessedPages(), getTotalPages());
			}
			logger.debug("Finished loading all data.");
			return result.stream();
		} catch (Exception e) {
			logger.error("Failed to complete parsing for {}", parserName);
			throw new RuntimeException(e);
		}
	}

}
