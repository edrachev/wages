package name.li.wages.plugin.hh;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Named;

import name.li.wages.normalizers.CyrillicRomanizer;
import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.JobEntry;

@Named
public class HhDataNormalizer {

	private final static Map<String, Month> monthDict = new HashMap<>() {
		private static final long serialVersionUID = 1L;

		{
			put("декабря", Month.DECEMBER);
			put("ноября", Month.NOVEMBER);
			put("октября", Month.OCTOBER);
			put("сентября", Month.SEPTEMBER);
			put("августа", Month.AUGUST);
			put("июля", Month.JULY);
			put("июня", Month.JUNE);
			put("мая", Month.MAY);
			put("апреля", Month.APRIL);
			put("марта", Month.MARCH);
			put("февраля", Month.FEBRUARY);
			put("января", Month.JANUARY);
		}
	};

	private SalaryParser salaryParser = new SalaryParser();
	private CyrillicRomanizer romanizer = new CyrillicRomanizer();

	public JobEntry normalize(HhRawJobEntry rawEntry) {
		var result = new JobEntry();

		var parsedSalary = salaryParser.parse(rawEntry.getSalary());
		if (parsedSalary.isPresent()) {
			var salary = parsedSalary.get();
			result.setMinSalary(salary.minSalary());
			result.setMaxSalary(salary.maxSalary());
			result.setSalaryCurrency(salary.currency().orElse(""));
		} else {
			result.addParsingProblem("Failed to parse salary, no suiting parsed for value: " + rawEntry.getSalary());
		}

		result.setCompany(rawEntry.getEmployer());
		result.setPosition(rawEntry.getTitle());
		result.setExternalId(rawEntry.getJobUrl());
		result.setLocation(
				new HhLocationParser(romanizer.romanize(rawEntry.getCountry().toLowerCase()))
						.parseLocation(romanizer.romanize(rawEntry.getLocation().toLowerCase())));

		parseDate(rawEntry.getCreationDate())
				.ifPresentOrElse(
						result::setCreatedOn,
						() -> result.addParsingProblem("Failed to parse creation date"));
		result.setExternalId(rawEntry.getJobUrl());
		result.setBuzzwords(Collections.singleton(Buzzword.of(rawEntry.getSearchedBy())));
		return result;
	}

	private Optional<LocalDate> parseDate(String dateStr) {
		try {
			var parts = dateStr.split(" ");
			var day = Integer.parseInt(parts[0]);
			var month = monthDict.get(parts[1]);
			return Optional.of(LocalDate.now().withMonth(month.getValue()).withDayOfMonth(day));
		} catch (Exception e) {
			return Optional.empty();
		}
	}

}
