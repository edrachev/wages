package name.li.wages.plugin.hh.process;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.HhDataNormalizer;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;

@Configuration
public class HhNormalizationProcessConfiguration {

	@Bean
	public ProcessDescriptor hhNormalizationProcess(
			HhDataNormalizer normalizer,
			HhRawJobEntryRepository rawJobEntryRepository,
			JobEntryRepository jobEntryRepository) {
		return new ProcessDescriptor() {

			@Override
			public String id() {
				return "hhNormalizationProcess";
			}

			@Override
			public Step createExecution(
					Optional<ContinuationToken> token,
					Optional<Generation> lastGeneration,
					Generation ongoingGeneration) {
				return new HhNormalizationStep(
						normalizer,
						ongoingGeneration,
						rawJobEntryRepository,
						jobEntryRepository);
			}

			@Override
			public boolean ownsToken(ContinuationToken token) {
				return false;
			}
		};
	}

}
