package name.li.wages.plugin.hh;

import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;

import name.li.wages.plugin.hh.HhClient.SearchConfiguration;

public class HhSearchParserFactory {

	public HhSearchInProgressParser newSearch(
			WebDriver searchResultPage,
			Supplier<WebDriver> seleniumDriverSupplier,
			SearchConfiguration searchConfiguration) {
		return HhSearchInProgressParser.startSearch(searchResultPage, seleniumDriverSupplier, searchConfiguration);
	}
}
