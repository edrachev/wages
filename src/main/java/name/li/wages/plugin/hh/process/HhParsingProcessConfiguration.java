package name.li.wages.plugin.hh.process;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.CompositeProcess;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.hh.HhClient;
import name.li.wages.plugin.hh.HhConfiguration;
import name.li.wages.plugin.hh.HhEnricher;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;
import name.li.wages.plugin.hh.HhSearchParserFactory;
import name.li.wages.selenium.SeleniumProvider;

@Configuration
public class HhParsingProcessConfiguration {

	@Bean
	public ProcessDescriptor create(
			HhRawJobEntryRepository rawJobEntryRepository,
			PlatformTransactionManager tm,
			HhSearchConfigurations searchConfigurations,
			SeleniumProvider selenium,
			HhConfiguration hhConfig) {
		var hhClient = new HhClient(selenium, new HhSearchParserFactory(), hhConfig);
		return new CompositeProcess("hhParsing",
				Arrays.asList(
						new CompositeProcess("hhSearchParsing",
								searchConfigurations.getConfigurations()
										.stream()
										.map(conf -> new HhSearchParsingProcess(
												rawJobEntryRepository,
												conf,
												hhClient,
												tm))
										.collect(Collectors.toList())),
						new HhDetailsParsingProcess(
								rawJobEntryRepository,
								tm,
								new HhEnricher(selenium::getPooledDriver))));
	}

}
