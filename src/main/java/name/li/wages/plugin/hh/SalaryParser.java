package name.li.wages.plugin.hh;

import java.util.Arrays;
import java.util.Optional;

import name.li.wages.plugin.api.Salary;
import name.li.wages.plugin.common.CompositeRegexParser;
import name.li.wages.plugin.common.CompositeRegexParser.SingleRegexParser;

public class SalaryParser {

	private CompositeRegexParser<Salary> compositeParser;

	public SalaryParser() {
		this.compositeParser = new CompositeRegexParser<>(Arrays.asList(
				new SingleRegexParser<Salary>(
						"з/п не указана",
						matcher -> Salary.builder().build()),
				new SingleRegexParser<Salary>(
						"^([\\d\\s]+)-([\\d\\s]+)\\s*(.*)$",
						matcher -> Salary.builder()
								.setMinSalary(Integer.parseInt(matcher.group(1).replace(" ", "")))
								.setMaxSalary(Integer.parseInt(matcher.group(2).replace(" ", "")))
								.setCurrency(matcher.group(3).trim().replace(".", "")).build()),
				new SingleRegexParser<Salary>(
						"^от\\s*([\\d\\s]+)\\s*(.*)$",
						matcher -> Salary.builder()
								.setMinSalary(Integer.parseInt(matcher.group(1).replace(" ", "")))
								.setCurrency(matcher.group(2).trim().replace(".", "")).build()),
				new SingleRegexParser<Salary>(
						"^до\\s*([\\d\\s]+)\\s*(.*)$",
						matcher -> Salary.builder()
								.setMaxSalary(Integer.parseInt(matcher.group(1).replace(" ", "")))
								.setCurrency(matcher.group(2).trim().replace(".", "")).build())));

	}

	public Optional<Salary> parse(String str) {
		return compositeParser.parse(str);
	}
}
