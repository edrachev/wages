package name.li.wages.plugin.hh.process;

import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.HhRawJobEntry;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;
import name.li.wages.plugin.hh.HhSearchInProgressParser;

public class HhSearchParsingStep implements Step {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private HhSearchInProgressParser parser;
	private HhRawJobEntryRepository rawJobEntryRepository;
	private String id;
	private TransactionTemplate tt;
	private Generation generation;

	@Override
	public String id() {
		return id;
	}

	@Override
	public GeneratorStatus status() {
		return parser.status();
	}

	@Override
	@Transactional
	public void start() throws ProcessError {
		logger.info("Starting step '{}'", id());
		try {
			tt.executeWithoutResult((transaction) -> {
				Stream<HhRawJobEntry> parsedItems = parser.generate();
				parsedItems.forEach(item -> {
					item.setGeneration(generation);
					rawJobEntryRepository.save(item);
				});
			});
		} catch (Exception e) {
			logger.info("Error during step '{}' : {}", id(), e.getMessage());
			throw new ProcessError(
					ContinuationToken.builder()
							.setStepId(id())
							.setValue(id())
							.build(),
					e);
		} finally {
			logger.info("Finished step '{}'", id());
		}
	}

	public HhSearchParsingStep(
			String id,
			HhRawJobEntryRepository rawJobEntryRepository,
			HhSearchInProgressParser parser,
			PlatformTransactionManager tm,
			Generation generation) {
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.parser = parser;
		this.id = id;
		this.generation = generation;
		this.tt = new TransactionTemplate(tm);
		this.tt.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("id", id)
				.toString();
	}

}
