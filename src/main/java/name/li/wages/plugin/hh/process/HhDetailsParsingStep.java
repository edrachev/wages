package name.li.wages.plugin.hh.process;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor.ContinuationToken;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.HhEnricher;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;

public class HhDetailsParsingStep implements Step {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private String id;
	private HhRawJobEntryRepository rawJobEntryRepository;
	private Generation generation;
	private HhEnricher enricher;

	private AtomicInteger totalItems = new AtomicInteger(0);
	private AtomicInteger itemsToProcess = new AtomicInteger(0);
	private TransactionTemplate tt;

	@Override
	public String id() {
		return id;
	}

	@Override
	public GeneratorStatus status() {
		return GeneratorStatus.simple(id(), () -> totalItems.get() - itemsToProcess.get(), () -> totalItems.get());
	}

	@Override
	public void start() throws ProcessError {
		logger.info("Starting step '{}'", id());
		try {
			var totalRawItemsForCurrentGeneration = rawJobEntryRepository
					.getAllEntriesForGeneration(generation.getId())
					.size();
			var allRawItemsForCurrentGeneration = rawJobEntryRepository
					.getEntriesWithoutDetailsForGeneration(generation.getId());

			this.totalItems.set(totalRawItemsForCurrentGeneration);
			this.itemsToProcess.set(allRawItemsForCurrentGeneration.size());

			for (var item : allRawItemsForCurrentGeneration) {
				tt.executeWithoutResult(perItemTransaction -> {
					enricher.enrich(item);
					item.setHasExtendedData(true);
					rawJobEntryRepository.save(item);
					itemsToProcess.decrementAndGet();
				});
			}
		} catch (Exception e) {
			logger.error("Failed to complete step '{}': {}", id(), e.getMessage());
			throw new ProcessError(
					ContinuationToken.builder()
							.setValue(id())
							.setStepId(id())
							.build(),
					e);
		} finally {
			logger.info("Finished step '{}'", id());
		}
	}

	public HhDetailsParsingStep(
			HhRawJobEntryRepository rawJobEntryRepository,
			PlatformTransactionManager tm,
			Generation generation,
			HhEnricher enricher,
			String id) {
		this.id = id;
		this.tt = new TransactionTemplate(tm);
		this.tt.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.generation = generation;
		this.enricher = enricher;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("id", id)
				.toString();
	}

}
