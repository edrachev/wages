package name.li.wages.plugin.hh.process;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.HhEnricher;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;

public class HhDetailsParsingProcess implements ProcessDescriptor {

	private HhRawJobEntryRepository rawJobEntryRepository;
	private HhEnricher enricher;
	private PlatformTransactionManager tm;

	@Override
	public String id() {
		return "Parsing job details";
	}

	@Override
	public Collection<ProcessDescriptor> subSteps() {
		return Collections.emptyList();
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration) {
		return new HhDetailsParsingStep(
				rawJobEntryRepository,
				tm,
				ongoingGeneration,
				enricher,
				id());
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return token.stepId().equals(id());
	}

	public HhDetailsParsingProcess(HhRawJobEntryRepository rawJobEntryRepository, PlatformTransactionManager tm,
			HhEnricher enricher) {
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.tm = tm;
		this.enricher = enricher;
	}

}
