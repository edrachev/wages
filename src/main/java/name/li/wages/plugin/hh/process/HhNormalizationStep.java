package name.li.wages.plugin.hh.process;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.HhDataNormalizer;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;

public class HhNormalizationStep implements Step {

	private HhDataNormalizer normalizer;
	private JobEntryRepository jobEntryRepository;
	private Generation ongoingGeneration;
	private volatile int state = 0;
	private HhRawJobEntryRepository rawJobEntryRepository;

	public HhNormalizationStep(
			HhDataNormalizer normalizer,
			Generation ongoingGeneration,
			HhRawJobEntryRepository rawJobEntryRepository,
			JobEntryRepository jobEntryRepository) {
		this.normalizer = normalizer;
		this.ongoingGeneration = ongoingGeneration;
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.jobEntryRepository = jobEntryRepository;
	}

	@Override
	public String id() {
		return getClass().getSimpleName();
	}

	@Override
	public GeneratorStatus status() {
		return GeneratorStatus.simple(id(), () -> state);
	}

	@Override
	public void start() throws ProcessError {
		state = 50;
		try {
			var items = rawJobEntryRepository.getLastVersionsOfRawEntities();
			items
					.stream()
					.map(normalizer::normalize)
					.forEach(item -> {
						item.setGeneration(ongoingGeneration);
						jobEntryRepository.save(item);
					});
		} finally {
			state = 100;
		}
	}

}
