package name.li.wages.plugin.hh.process;

import java.util.Optional;

import org.springframework.transaction.PlatformTransactionManager;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.hh.HhClient;
import name.li.wages.plugin.hh.HhClient.SearchConfiguration;
import name.li.wages.plugin.hh.HhRawJobEntryRepository;

public class HhSearchParsingProcess implements ProcessDescriptor {

	private HhRawJobEntryRepository rawJobEntryRepository;
	private SearchConfiguration searchConfig;
	private HhClient hhClient;
	private PlatformTransactionManager tm;

	public HhSearchParsingProcess(
			HhRawJobEntryRepository rawJobEntryRepository,
			SearchConfiguration searchConfig,
			HhClient hhClient,
			PlatformTransactionManager tm) {
		this.rawJobEntryRepository = rawJobEntryRepository;
		this.searchConfig = searchConfig;
		this.hhClient = hhClient;
		this.tm = tm;
	}

	@Override
	public String id() {
		return "hh search results parsing " + searchConfig.term() + " in " + searchConfig.location();
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation generation) {
		return new HhSearchParsingStep(
				id(),
				rawJobEntryRepository,
				hhClient.getSearchResultForLocationAndTerm(searchConfig, lastGeneration),
				tm,
				generation);
	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return id().equals(token.stepId());
	}

}
