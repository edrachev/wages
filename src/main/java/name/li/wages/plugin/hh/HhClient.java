package name.li.wages.plugin.hh;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auto.value.AutoValue;

import name.li.wages.plugin.api.process.Generation;
import name.li.wages.selenium.SeleniumProvider;

public class HhClient {

	@AutoValue
	public static abstract class SearchConfiguration {

		public abstract Location location();

		public abstract String term();

		public static Builder builder() {
			return new AutoValue_HhClient_SearchConfiguration.Builder();
		}

		@AutoValue.Builder
		public static abstract class Builder {
			public abstract Builder setLocation(Location location);

			public abstract Builder setTerm(String term);

			public abstract SearchConfiguration build();
		}

	}

	public enum Location {
		RYAZAN(77, "Russia"),
		MOSCOW(1, "Russia"),
		KAZAKHSTAN(40, "Kazakhstan");

		private final int code;
		private final String country;

		Location(int code, String country) {
			this.code = code;
			this.country = country;
		}

		public int code() {
			return this.code;
		}

		public String country() {
			return this.country;
		}
	}

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private final SeleniumProvider selenium;
	private final HhSearchParserFactory searchParserFactory;
	private HhConfiguration config;

	public HhClient(
			SeleniumProvider selenium,
			HhSearchParserFactory searchParserFactory,
			HhConfiguration config) {
		this.selenium = selenium;
		this.searchParserFactory = searchParserFactory;
		this.config = config;
	}

	public HhSearchInProgressParser getSearchResultForLocationAndTerm(
			SearchConfiguration config,
			Optional<Generation> lastGeneration) {
		return searchParserFactory.newSearch(
				getSearchResult(config.location(), config.term(), lastGeneration),
				this::getNewDriver,
				config);
	}

	private WebDriver getNewDriver() {
		return selenium.getNewDriver();
	}

	private WebDriver getSearchResult(Location location, String term, Optional<Generation> lastGeneration) {
		try {
			var defaultSearchPeriodValue = config.defaultSearchPeriod();
			var searchPeriod = lastGeneration.map(gen -> {
				var daysSinceLastGeneration = getDaysSinceLastGeneration(gen);
				logger.info("Last generation was performed on {}, {} days ago, using {} as search_period",
						gen.getDate(), daysSinceLastGeneration, daysSinceLastGeneration);
				return daysSinceLastGeneration;
			}).orElseGet(() -> {
				logger.info("No last generation found, using {} as search_period", defaultSearchPeriodValue);
				return defaultSearchPeriodValue;
			});
			var uri = UriBuilder
					.fromPath(hhRelativeUrl("search/vacancy"))
					.queryParam("st", "searchVacancy")
					.queryParam("search_period", searchPeriod)
					.queryParam("area", location.code())
					.queryParam("text", term).build();
			var driver = selenium.getNewDriver();
			driver.get(uri.toString());
			return driver;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private long getDaysSinceLastGeneration(Generation generation) {
		return Math.max(ChronoUnit.DAYS.between(generation.getDate(), LocalDate.now()), 1);
	}

	private static String hhRelativeUrl(String relativePart) {
		return "https://hh.ru/" + relativePart;
	}

}
