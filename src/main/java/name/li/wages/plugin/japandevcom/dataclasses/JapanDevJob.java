package name.li.wages.plugin.japandevcom.dataclasses;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_JapanDevJob.Builder.class)
public abstract class JapanDevJob {

	public abstract int id();

	public abstract String type();

	public abstract JapanDevJobAttributes attributes();

	public static Builder builder() {
		return new AutoValue_JapanDevJob.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
	public abstract static class Builder {
		public abstract Builder setId(int id);

		public abstract Builder setType(String id);

		public abstract Builder setAttributes(JapanDevJobAttributes attributes);

		public abstract JapanDevJob build();
	}
}
