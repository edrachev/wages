package name.li.wages.plugin.japandevcom;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import name.li.wages.plugin.japandevcom.dataclasses.JapanDevResponse;

public class JapanDevResponseParser {

	public JapanDevResponse parse(InputStream is) throws JsonParseException, JsonMappingException, IOException {
		return new ObjectMapper().readValue(is, JapanDevResponse.class);
	}

}
