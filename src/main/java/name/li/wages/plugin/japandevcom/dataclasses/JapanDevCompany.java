package name.li.wages.plugin.japandevcom.dataclasses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@JsonIgnoreProperties(ignoreUnknown = true)
@AutoValue
@JsonDeserialize(builder = AutoValue_JapanDevCompany.Builder.class)
public abstract class JapanDevCompany {

	public abstract String name();

	public static Builder builder() {
		return new AutoValue_JapanDevCompany.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(buildMethodName = "build", withPrefix = "set")
	@JsonIgnoreProperties(ignoreUnknown = true) // many properties have only test data in japandev response
	public abstract static class Builder {
		public abstract Builder setName(String name);

		public abstract JapanDevCompany build();
	}

}
