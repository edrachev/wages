package name.li.wages.plugin.japandevcom;

import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import name.li.wages.plugin.api.Buzzword;
import name.li.wages.plugin.api.JobEntry;

@Named
public class JapanDevParser {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private final JapanDevClient japanDevClient;
	private final JapanDevDateParser dateParser;
	private final JapanDevLocationParser locationParser;

	@Inject
	public JapanDevParser(JapanDevClient japanDevClient) {
		this.japanDevClient = japanDevClient;
		this.dateParser = new JapanDevDateParser();
		this.locationParser = new JapanDevLocationParser();
	}

	public Stream<JobEntry> generate()
			throws IOException {
		try {
			return japanDevClient.getData().data()
					.stream()
					.filter(e -> Optional.ofNullable(e.attributes().title())
							.map(title -> !title.toLowerCase().contains("example")).orElse(false))
					.map(responseItem -> {
						var job = new JobEntry();
						Optional.ofNullable(responseItem.attributes().location())
								.flatMap(locationParser::parse)
								.ifPresent(job::setLocation);
						Optional.ofNullable(responseItem.attributes().company())
								.ifPresent(company -> job.setCompany(company.name()));
						Optional.ofNullable(responseItem.attributes().jobPostDate())
								.ifPresent(date -> {
									try {
										job.setCreatedOn(dateParser.parse(date));
									} catch (DateTimeParseException e) {
										job.addParsingProblem(e.getMessage());
									}
								});
						job.setExternalId(
								String.format("japan-dev.com/jobs/%s/%s", job.getCompany(),
										responseItem.attributes().slug()));
						job.setMinSalary(
								Optional.ofNullable(responseItem.attributes().salaryMin())
										.map(i -> i * 100_000)
										.map(OptionalInt::of)
										.orElseGet(OptionalInt::empty));
						job.setMaxSalary(
								Optional.ofNullable(responseItem.attributes().salaryMax())
										.map(i -> i * 100_000)
										.map(OptionalInt::of)
										.orElseGet(OptionalInt::empty));
						job.setPosition(Optional.ofNullable(responseItem.attributes().title()).orElse(""));
						job.setBuzzwords(Optional.ofNullable(responseItem.attributes().technologies())
								.map(technologies -> technologies.split(","))
								.stream()
								.flatMap(Stream::of)
								.map(String::trim)
								.map(Buzzword::of)
								.collect(Collectors.toSet()));
						job.setSalaryCurrency("yen");
						return job;
					});
		} catch (Exception e) {
			logger.error("Failed to fetch japanDev data");
			throw new RuntimeException(e);
		}
	}

}
