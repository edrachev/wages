package name.li.wages.plugin.japandevcom.process;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.ProcessDescriptor;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.japandevcom.JapanDevParser;

@Configuration
public class JapanDevProcessConfiguration {

	@Bean
	public ProcessDescriptor japanDevProcess(
			JapanDevParser parser,
			JobEntryRepository jobEntryRepository,
			PlatformTransactionManager tm) {
		return new ProcessDescriptor() {

			@Override
			public String id() {
				return "japanDevParsingProcess";
			}

			@Override
			public Step createExecution(
					Optional<ContinuationToken> token,
					Optional<Generation> lastGeneration,
					Generation ongoingGeneration) {
				return new JapanDevParsingStep(
						ongoingGeneration,
						parser,
						jobEntryRepository,
						new TransactionTemplate(tm));
			}

			@Override
			public boolean ownsToken(ContinuationToken token) {
				return false;
			}

		};
	}

}
