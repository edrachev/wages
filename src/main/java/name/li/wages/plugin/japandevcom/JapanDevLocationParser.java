package name.li.wages.plugin.japandevcom;

import java.util.Arrays;
import java.util.Optional;

import name.li.wages.plugin.api.Location;
import name.li.wages.plugin.common.CompositeRegexParser;

public class JapanDevLocationParser {

	private CompositeRegexParser<Location> compositeParser;

	public JapanDevLocationParser() {
		this.compositeParser = new CompositeRegexParser<Location>(
				Arrays.asList(
						new CompositeRegexParser.SingleRegexParser<Location>("^([A-z]+)\\s*\\(([A-z]*)\\)$",
								matcher -> Location.create("japan", matcher.group(1), matcher.group(2))),
						new CompositeRegexParser.SingleRegexParser<Location>("^([A-z]+)$",
								matcher -> Location.create("japan", matcher.group(1), ""))));
	}

	public Optional<Location> parse(String str) {
		return compositeParser.parse(str);
	}

}
