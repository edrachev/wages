package name.li.wages.plugin.japandevcom.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionTemplate;

import name.li.wages.persistence.JobEntryRepository;
import name.li.wages.plugin.api.GeneratorStatus;
import name.li.wages.plugin.api.process.Generation;
import name.li.wages.plugin.api.process.Step;
import name.li.wages.plugin.japandevcom.JapanDevParser;

public class JapanDevParsingStep implements Step {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private JapanDevParser parser;
	private JobEntryRepository jobEntryRepository;
	private Generation ongoingGeneration;
	private TransactionTemplate tt;
	private volatile int progress = 0;

	public JapanDevParsingStep(
			Generation ongoingGeneration,
			JapanDevParser parser,
			JobEntryRepository jobEntryRepository,
			TransactionTemplate tt) {
		this.ongoingGeneration = ongoingGeneration;
		this.parser = parser;
		this.jobEntryRepository = jobEntryRepository;
		this.tt = tt;
	}

	@Override
	public String id() {
		return "japanDevParsing";
	}

	@Override
	public GeneratorStatus status() {
		return GeneratorStatus.simple(() -> progress);
	}

	@Override
	public void start() throws ProcessError {
		logger.info("Starting step '{}'", id());
		tt.executeWithoutResult(transactionStatus -> {
			try {
				progress = 50;
				parser.generate()
						.forEach(item -> {
							item.setGeneration(ongoingGeneration);
							jobEntryRepository.save(item);
						});
			} catch (Exception e) {
				logger.info("Error during step '{}': ", id(), e.getMessage());
				throw new RuntimeException(e);
			} finally {
				progress = 100;
				logger.info("Finished step '{}'", id());
			}
		});
	}

}
