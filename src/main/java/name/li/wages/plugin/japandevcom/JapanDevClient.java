package name.li.wages.plugin.japandevcom;

import java.io.IOException;
import java.io.InputStream;

import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import name.li.wages.plugin.japandevcom.dataclasses.JapanDevResponse;

@Named
public class JapanDevClient {

	private JapanDevResponseParser parser;
	private Client restClient;

	public JapanDevClient() {
		this.parser = new JapanDevResponseParser();
		this.restClient = ClientBuilder.newClient();
	}

	public JapanDevResponse getData() throws JsonParseException, JsonMappingException, IOException {
		return parser.parse(
				restClient.target("https://japan-dev.com/api/v1/jobs")
						.request(MediaType.APPLICATION_JSON).get(InputStream.class));

	}
}
