package name.li.wages.plugin.api;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public interface GeneratorStatus {

	int percentsCompleted();

	boolean working();

	default String description() {
		return "";
	}

	default Collection<GeneratorStatus> substatuses() {
		return Collections.emptyList();
	}

	public class CompositeStatus implements GeneratorStatus {

		private Collection<GeneratorStatus> substatuses;
		private String description;

		CompositeStatus(String description, Collection<GeneratorStatus> substatuses) {
			this.substatuses = substatuses;
			this.description = description;
		}

		@Override
		public int percentsCompleted() {
			return substatuses.stream().collect(Collectors.averagingInt(GeneratorStatus::percentsCompleted)).intValue();
		}

		@Override
		public boolean working() {
			return substatuses.stream().filter(GeneratorStatus::working).findAny().map(e -> true).orElse(false);
		}

		@Override
		public Collection<GeneratorStatus> substatuses() {
			return substatuses;
		}

		@Override
		public String description() {
			return description;
		}

	}

	public class SimpleStatus implements GeneratorStatus {

		private Supplier<Integer> progressSupplier;
		private String description;

		public SimpleStatus(String description, Supplier<Integer> progressSupplier) {
			this.progressSupplier = progressSupplier;
			this.description = description;
		}

		@Override
		public String description() {
			return description;
		}

		@Override
		public int percentsCompleted() {
			return progressSupplier.get();
		}

		@Override
		public boolean working() {
			return percentsCompleted() >= 0 && percentsCompleted() < 100;
		}

	}

	public static GeneratorStatus simple(String description, Supplier<Integer> sofar, Supplier<Integer> total) {
		return simple(description, () -> {
			var totalNum = total.get();
			if (totalNum > 0) {
				return sofar.get() * 100 / totalNum;
			} else {
				return 0;
			}
		});
	}

	public static GeneratorStatus simple(Supplier<Integer> progressSupplier) {
		return new SimpleStatus("", progressSupplier);
	}

	public static GeneratorStatus simple(String description, Supplier<Integer> progressSupplier) {
		return new SimpleStatus(description, progressSupplier);
	}

	public static GeneratorStatus composite(String description, Collection<GeneratorStatus> substatuses) {
		return new CompositeStatus(description, substatuses);
	}

}
