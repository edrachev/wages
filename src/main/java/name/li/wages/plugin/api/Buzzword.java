package name.li.wages.plugin.api;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Buzzword {

	public abstract String getValue();

	public static Buzzword of(String buzzword) {
		return new AutoValue_Buzzword(buzzword);
	}

}
