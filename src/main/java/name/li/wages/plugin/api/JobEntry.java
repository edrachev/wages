package name.li.wages.plugin.api;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.OptionalInt;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import name.li.wages.plugin.api.process.Generation;

@Entity
@Table(name = "job_entry")
public class JobEntry {

	@Id
	@Column(name = "id")
	private String externalId;

	@Column(name = "company")
	private String company;

	@Column(name = "location")
	private Location location;

	@Column(name = "position")
	private String position;

	@Convert(converter = OptionalIntConverter.class)
	@Column(name = "salary_min")
	private OptionalInt minSalary = OptionalInt.empty();

	@Convert(converter = OptionalIntConverter.class)
	@Column(name = "salary_max")
	private OptionalInt maxSalary = OptionalInt.empty();

	@Column(name = "salary_currency")
	private String salaryCurrency;

	@Column(name = "created_on")
	private LocalDate createdOn;

	@ElementCollection
	private Collection<String> parsingProblems;

	@ManyToOne
	private Generation generation;

	@Convert(converter = BuzzWordFlattener.class)
	private Set<Buzzword> buzzwords = Collections.emptySet();

	public Generation getGeneration() {
		return generation;
	}

	public void setGeneration(Generation generation) {
		this.generation = generation;
	}

	public Set<Buzzword> getBuzzwords() {
		return Objects.requireNonNullElse(buzzwords, Collections.emptySet());
	}

	public void setBuzzwords(Set<Buzzword> buzzwords) {
		this.buzzwords = buzzwords;
	}

	public LocalDate getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDate createdOn) {
		this.createdOn = createdOn;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public Collection<String> getParsingProblems() {
		return Objects.requireNonNullElse(parsingProblems, Collections.emptyList());
	}

	public void setParsingProblems(Collection<String> parsingProblems) {
		this.parsingProblems = parsingProblems;
	}

	public String getSalaryCurrency() {
		return salaryCurrency;
	}

	public void setSalaryCurrency(String salaryCurrency) {
		this.salaryCurrency = salaryCurrency;
	}

	public OptionalInt getMinSalary() {
		return minSalary;
	}

	public void setMinSalary(OptionalInt minSalary) {
		this.minSalary = minSalary;
	}

	public OptionalInt getMaxSalary() {
		return maxSalary;
	}

	public void setMaxSalary(OptionalInt maxSalary) {
		this.maxSalary = maxSalary;
	}

	public void addParsingProblem(String problem) {
		if (this.parsingProblems == null) {
			this.parsingProblems = new ArrayList<>();
		}
		this.parsingProblems.add(problem);
	}

	@Override
	public String toString() {
		return "JobEntry [externalId=" + externalId + ", company=" + company + ", location=" + location + ", position="
				+ position
				+ ", minSalary=" + minSalary + ", maxSalary=" + maxSalary + ", salaryCurrency=" + salaryCurrency
				+ ", createdOn=" + createdOn + ", parsingProblems=" + parsingProblems
				+ ", generation=" + generation + ", buzzwords=" + buzzwords + "]";
	}

}
