package name.li.wages.plugin.api.process;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.GeneratorStatus;

public class SkippedStep implements Step {

	private String id;

	@Override
	public String id() {
		return id;
	}

	@Override
	public GeneratorStatus status() {
		return GeneratorStatus.simple(() -> 100);
	}

	@Override
	public void start() throws ProcessError {
	}

	public SkippedStep(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(getClass())
				.add("id", id)
				.toString();
	}

}
