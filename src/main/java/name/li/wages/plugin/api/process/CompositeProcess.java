package name.li.wages.plugin.api.process;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.Streams;

public class CompositeProcess implements ProcessDescriptor {

	private Collection<ProcessDescriptor> steps;
	private String id;

	public CompositeProcess(String id, Collection<ProcessDescriptor> steps) {
		this.id = id;
		this.steps = steps;
	}

	@Override
	public String id() {
		return id;
	}

	public Collection<ProcessDescriptor> subSteps() {
		return steps;
	}

	@Override
	public Step createExecution(
			Optional<ContinuationToken> mayBeToken,
			Optional<Generation> lastGeneration,
			Generation currentGeneration) {

		if (mayBeToken.isPresent()) {
			var token = mayBeToken.get();
			Step continueFromStep = null;
			Collection<Step> restSteps = new ArrayList<>();
			var pastSteps = new ArrayList<Step>();

			for (var process : steps) {
				if (process.ownsToken(token)) {
					continueFromStep = process.createExecution(Optional.of(token), lastGeneration, currentGeneration);
				} else if (continueFromStep == null) {
					pastSteps.add(Step.skipped(process.id()));
				} else {
					restSteps.add(process.createExecution(Optional.empty(), lastGeneration, currentGeneration));
				}
			}
			if (continueFromStep == null) {
				throw new RuntimeException("Could not find owner for token " + token);
			}
			return Step.composite(id(),
					Streams.concat(
							pastSteps.stream(),
							Stream.of(continueFromStep),
							restSteps.stream()).collect(Collectors.toList()));
		} else {
			return Step.composite(
					id(),
					steps.stream()
							.map(e -> e.createExecution(Optional.empty(), lastGeneration, currentGeneration))
							.collect(Collectors.toList()));
		}

	}

	@Override
	public boolean ownsToken(ContinuationToken token) {
		return steps.stream().filter(p -> p.ownsToken(token)).findAny().isPresent();
	}

}