package name.li.wages.plugin.api.process;

import java.util.Collection;
import java.util.stream.Collectors;

import com.google.common.base.MoreObjects;

import name.li.wages.plugin.api.GeneratorStatus;

class CompositeStep implements Step {

	private final Collection<Step> steps;
	private final String id;

	@Override
	public String id() {
		return id;
	}

	@Override
	public GeneratorStatus status() {
		return GeneratorStatus.composite(
				id(),
				steps().stream().map(Step::status).collect(Collectors.toList()));
	}

	@Override
	public Collection<Step> steps() {
		return steps;
	}

	@Override
	public void start() throws ProcessError {
		try {
			for (var step : steps) {
				step.start();
			}
		} catch (ProcessError e) {
			throw e;
		}

	}

	public CompositeStep(String id, Collection<Step> substeps) {
		this.steps = substeps;
		this.id = id;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this.getClass())
				.add("id", id)
				.add("steps", steps)
				.toString();
	}
}