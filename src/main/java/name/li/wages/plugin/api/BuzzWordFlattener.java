package name.li.wages.plugin.api;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class BuzzWordFlattener implements AttributeConverter<Set<Buzzword>, String> {

	@Override
	public String convertToDatabaseColumn(Set<Buzzword> list) {
		return String.join(",", list.stream().map(e -> e.getValue()).collect(Collectors.toList()));
	}

	@Override
	public Set<Buzzword> convertToEntityAttribute(String joined) {
		return Arrays.asList(joined.split(",")).stream().map(Buzzword::of).collect(Collectors.toSet());
	}

}