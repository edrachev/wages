package name.li.wages.plugin.api;

import java.util.Optional;
import java.util.OptionalInt;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Salary {

	public abstract OptionalInt minSalary();

	public abstract OptionalInt maxSalary();

	public abstract Optional<String> currency();

	public static Salary.Builder builder() {
		return new AutoValue_Salary.Builder();
	}

	@AutoValue.Builder
	public static abstract class Builder {

		public abstract Salary.Builder setMinSalary(int minSalary);

		public abstract Salary.Builder setMinSalary(OptionalInt minSalary);

		public abstract Salary.Builder setMaxSalary(int maxSalary);

		public abstract Salary.Builder setMaxSalary(OptionalInt maxSalary);

		public abstract Salary.Builder setCurrency(String currency);

		public abstract Salary build();
	}
}