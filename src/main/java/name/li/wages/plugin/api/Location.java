package name.li.wages.plugin.api;

import javax.persistence.Embeddable;

@Embeddable
public class Location {

	private String city = "";
	private String country = "";
	private String area = "";

	public String getArea() {
		return area;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public static Location create(String country, String city, String area) {
		var res = new Location();
		res.setCity(city);
		res.setArea(area);
		res.setCountry(country);
		return res;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Location [city=" + city + ", country=" + country + ", area=" + area + "]";
	}

}
