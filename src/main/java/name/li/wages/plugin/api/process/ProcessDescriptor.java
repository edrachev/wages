package name.li.wages.plugin.api.process;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import com.google.auto.value.AutoValue;

public interface ProcessDescriptor {

	@AutoValue
	public static abstract class ContinuationToken {
		public abstract String value();

		public abstract String stepId();

		public static Builder builder() {
			return new AutoValue_ProcessDescriptor_ContinuationToken.Builder();
		}

		@AutoValue.Builder
		public abstract static class Builder {
			public abstract Builder setValue(String value);

			public abstract Builder setStepId(String stepId);

			public abstract ContinuationToken build();
		}
	}

	String id();

	default Collection<ProcessDescriptor> subSteps() {
		return Collections.emptyList();
	}

	Step createExecution(
			Optional<ContinuationToken> token,
			Optional<Generation> lastGeneration,
			Generation ongoingGeneration);

	boolean ownsToken(ContinuationToken token);
}
