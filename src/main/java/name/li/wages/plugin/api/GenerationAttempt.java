package name.li.wages.plugin.api;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "generation_attempt")
public final class GenerationAttempt {

	@Id
	@Column(name = "id")
	private String id = UUID.randomUUID().toString();

	@Column(name = "description", length = 10_000)
	private String description;

	@Column(name = "instant")
	private Instant instant;

	@Column(name = "date")
	private LocalDate date;

	public LocalDate calcDate() {
		return LocalDate.from(instant);
	}

	public String getId() {
		return id;
	}

	public LocalDate getDate() {
		return date;
	}

	public String getDescription() {
		return description;
	}

	protected GenerationAttempt() {
	}

	private GenerationAttempt(String description, String id, Instant date) {
		this.instant = date;
		this.id = id;
		this.description = description;
		this.date = LocalDate.ofInstant(instant, ZoneOffset.UTC);
	}

	public static GenerationAttempt create(String description) {
		return new GenerationAttempt(description, UUID.randomUUID().toString(), Instant.now());
	}

}
