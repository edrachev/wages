package name.li.wages.plugin.api.process;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import name.li.wages.plugin.api.GeneratorStatus;

@Configuration
public class DummyProcessConfig {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Bean
	public ProcessDescriptor dummy() {
		return new CompositeProcess("root", Arrays.asList(
				counting("counter1", 10, 5),
				counting("counter2", 5, 3)));
	}

	private ProcessDescriptor counting(String id, int countTo, int errorOn) {
		var counter = new AtomicInteger(0);
		return new ProcessDescriptor() {

			@Override
			public boolean ownsToken(ContinuationToken token) {
				return token.stepId().equals(id);
			}

			@Override
			public String id() {
				return id;
			}

			@Override
			public Step createExecution(Optional<ContinuationToken> token, Optional<Generation> lastGeneration,
					Generation ongoingGeneration) {
				token.ifPresent(val -> counter.set(Integer.valueOf(val.value())));
				return new Step() {

					@Override
					public GeneratorStatus status() {
						return GeneratorStatus.simple(id, () -> counter.get(), () -> countTo);
					}

					@Override
					public void start() throws ProcessError {
						logger.info("Starting '{}' from {}", id(), counter.get());
						while (counter.get() < countTo) {
							counter.incrementAndGet();
							if (counter.get() == errorOn) {
								logger.info("Erroring '{}' on {}", id(), counter.get());
								throw new ProcessError(
										ContinuationToken.builder()
												.setStepId(id())
												.setValue(String.valueOf(counter.get()))
												.build(),
										new RuntimeException("ooopsie"));
							}
						}
						logger.info("=======");
						logger.info("=======");
						logger.info("COMLETELY COMPLETED! " + id());
						logger.info("=======");
						logger.info("=======");
						counter.set(0);
					}

					@Override
					public String id() {
						return id;
					}
				};
			}
		};
	}

}
