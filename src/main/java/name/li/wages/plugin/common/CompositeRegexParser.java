package name.li.wages.plugin.common;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CompositeRegexParser<T> {

	public static class SingleRegexParser<Output> {

		private Pattern regex;
		private Function<Matcher, Output> func;

		public SingleRegexParser(String regex, Function<Matcher, Output> func) {
			this.regex = Pattern.compile(regex);
			this.func = func;
		}

		private Optional<Output> tryParse(String str) {
			var matcher = regex.matcher(str);
			if (matcher.find()) {
				return Optional.of(func.apply(matcher));
			} else {
				return Optional.empty();
			}
		}
	}

	private Collection<SingleRegexParser<T>> parsers;

	public CompositeRegexParser(Collection<SingleRegexParser<T>> parsers) {
		this.parsers = parsers;
	}

	public Optional<T> parse(String str) {
		return parsers.stream().map(parser -> parser.tryParse(str)).flatMap(Optional::stream).findAny();
	}

}
