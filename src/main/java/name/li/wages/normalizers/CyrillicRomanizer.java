package name.li.wages.normalizers;

import java.util.Map;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableMap;

public class CyrillicRomanizer {

	private Map<Character, String> substitutions = ImmutableMap.<Character, String>builder()
			.put('а', "a")
			.put('б', "b")
			.put('в', "v")
			.put('г', "g")
			.put('д', "d")
			.put('е', "e")
			.put('ё', "yo")
			.put('ж', "zh")
			.put('з', "z")
			.put('и', "i")
			.put('й', "y")
			.put('к', "k")
			.put('л', "l")
			.put('м', "m")
			.put('н', "n")
			.put('о', "o")
			.put('п', "p")
			.put('р', "r")
			.put('с', "s")
			.put('т', "t")
			.put('у', "u")
			.put('ф', "f")
			.put('х', "h")
			.put('ц', "ts")
			.put('ч', "ch")
			.put('ш', "sh")
			.put('щ', "sch")
			.put('ъ', "")
			.put('ы', "y")
			.put('ь', "")
			.put('э', "e")
			.put('ю', "yu")
			.put('я', "ya")
			.build();

	public String romanize(String str) {
		return str.chars().mapToObj(i -> substitutions.getOrDefault((char) i, String.valueOf((char) i)))
				.collect(Collectors.joining());

	}
}
