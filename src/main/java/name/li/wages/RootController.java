package name.li.wages;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import name.li.wages.plugin.hh.HhRawJobEntry;

@RestController
public class RootController {

	@Autowired
	private EntityManager em;

	@GetMapping("showHhEntity/{id}")
	@Transactional
	public String foo(@PathVariable String id) {
		return em.find(HhRawJobEntry.class, id).toString();
	}

}
